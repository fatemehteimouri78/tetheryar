import 'package:shared_preferences/shared_preferences.dart';

Future<bool> checkLogin() async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  String token = pref.getString("token") ?? "";
  if(token != ""){
    return true;
  }else{
    return false;
  }
}