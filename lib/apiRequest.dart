import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Screens/SplashScreen.dart';

enum RequestTypes { POST, GET, DEL, PUT, PATCH }
sendRequest(BuildContext context, RequestTypes type, String url, dynamic body,
    Function function,
    {bool checkResponse = true, Function? onError,bool hasToken = true}) async {
  String baseUrl = 'http://168.119.212.5/~linomsit/';
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token") ?? "";
  String userId = prefs.getString("userId") ?? "";
  print(token);
  print(type);
  Map<String, String> headers;
  if(hasToken){
     headers = {
      'Accept': 'application/json',
      'Authorization': "Bearer "+token,
      'userId': userId,
    };
  }else{
    headers = {
      'Accept': 'application/json',
    };
  }
  print("token");
  print(token);
  print("userId");
  print(userId);
  var dio = Dio();
  var options = Options(
      followRedirects: false,
      validateStatus: (status) {
        return (status! < 600);
      },
      headers: headers);
  try {
    Response response;
    switch (type) {
      case RequestTypes.POST:
        response = await dio.post(
          baseUrl + url,
          options: options,
          data: body,
        );
        break;
      case RequestTypes.GET:
        response = await dio.get(
          baseUrl + url,
          options: options,
          queryParameters: body,
        );
        break;
      case RequestTypes.DEL:
        response = await dio.delete(
            baseUrl + url,
            options: options,
            data: body
        );
        break;
      case RequestTypes.PUT:
        response = await dio.put(
          baseUrl + url,
          options: options,
          data: body,
        );
        break;
      case RequestTypes.PATCH:
        response = await dio.patch(
          baseUrl + url,
          options: options,
          data: body,
        );
        break;
    }
    print(response.requestOptions.uri);
    print(response.statusCode);
    print(response.requestOptions.uri.toString());
    print(response.requestOptions.headers.toString());
    print(response.requestOptions.toString());
    print(type);
    log(

        response.toString());
    if (response.statusCode == 401) {
      prefs.setString("token", "");
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => SplashScreen(title: '',)),
              (route) => false);
    } else {
      if (response.requestOptions.data is FormData) {
        print((response.requestOptions.data as FormData).fields);
        print((response.requestOptions.data as FormData).files);
      } else {
        print(response.requestOptions.data.toString());
      }
      print(response.requestOptions.uri.toString());
      print(response.toString());
      if (checkResponse) {
        if ((response.statusCode! / 100).round() == 2) {
          function(response);
        } else {
          if (onError != null) {
            onError();
          }
          // ScaffoldMessenger.of(context)
          //   ..showSnackBar(
          //     SnackBar(
          //       content: response.data["errors"]["mobile"].toString() == "null" ?
          //       Text(response.data["errors"]["captcha"].toString(), textAlign: TextAlign.center)
          //           :
          //       Text(response.data["errors"]["mobile"].toString(), textAlign: TextAlign.center),
          //       backgroundColor: Theme.of(context).errorColor,
          //     ),
          //   );
        }
      } else {
        function(response);
      }
    }
  } catch (error) {
    if (onError != null) {
      onError();
    }
    print(error);
    // ScaffoldMessenger.of(context)
    //   ..showSnackBar(
    //     SnackBar(
    //       content: Text(error.toString(), textAlign: TextAlign.center),
    //       backgroundColor: Theme.of(context).errorColor,
    //     ),
    //   );
  }
}
