// To parse this JSON data, do
//
//     final walletObject = walletObjectFromJson(jsonString);

import 'dart:convert';

WalletObject walletObjectFromJson(String str) => WalletObject.fromJson(json.decode(str));

String walletObjectToJson(WalletObject data) => json.encode(data.toJson());

class WalletObject {
  WalletObject({
    this.id,
    this.walletType,
    this.walletAddress,
    this.walletQrcode,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? walletType;
  String? walletAddress;
  String? walletQrcode;
  dynamic? createdAt;
  dynamic? updatedAt;

  factory WalletObject.fromJson(Map<String, dynamic> json) => WalletObject(
    id: json["id"],
    walletType: json["wallet_type"],
    walletAddress: json["wallet_address"],
    walletQrcode: json["wallet_qrcode"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "wallet_type": walletType,
    "wallet_address": walletAddress,
    "wallet_qrcode": walletQrcode,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
