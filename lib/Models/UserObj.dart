// To parse this JSON data, do
//
//     final userObject = userObjectFromJson(jsonString);

import 'dart:convert';

UserObject userObjectFromJson(String str) => UserObject.fromJson(json.decode(str));

String userObjectToJson(UserObject data) => json.encode(data.toJson());

class UserObject {
  UserObject({
    this.user,
    this.authentication,
    this.authMessages,
    this.invitedUserNumber,
  });

  User? user;
  Authentication? authentication;
  List<dynamic>? authMessages;
  int? invitedUserNumber;

  factory UserObject.fromJson(Map<String, dynamic> json) => UserObject(
    user: User.fromJson(json["user"]),
    authentication: json["authentication"] is String ? null : Authentication.fromJson(json["authentication"]),
    authMessages: List<dynamic>.from(json["authMessages"].map((x) => x)),
    invitedUserNumber: json["invitedUserNumber"],
  );

  Map<String, dynamic> toJson() => {
    "user": user!.toJson(),
    "authentication": authentication!.toJson(),
    "authMessages": List<dynamic>.from(authMessages!.map((x) => x)),
    "invitedUserNumber": invitedUserNumber,
  };
}

class Authentication {
  Authentication({
    this.id,
    this.userId,
    this.firstName,
    this.lastName,
    this.nationalCode,
    this.phoneNumber,
    this.callTime,
    this.nationalCardPicture,
    this.selfiPicture,
    this.statuss,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? userId;
  String? firstName;
  String? lastName;
  String? nationalCode;
  String? phoneNumber;
  String? callTime;
  String? nationalCardPicture;
  dynamic? selfiPicture;
  String? statuss;
  dynamic? createdAt;
  dynamic? updatedAt;

  factory Authentication.fromJson(Map<String, dynamic> json) => Authentication(
    id: json["id"],
    userId: json["user_id"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    nationalCode: json["nationalCode"],
    phoneNumber: json["phoneNumber"],
    callTime: json["callTime"],
    nationalCardPicture: json["nationalCardPicture"],
    selfiPicture: json["selfiPicture"],
    statuss: json["status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "first_name": firstName,
    "last_name": lastName,
    "nationalCode": nationalCode,
    "phoneNumber": phoneNumber,
    "callTime": callTime,
    "nationalCardPicture": nationalCardPicture,
    "selfiPicture": selfiPicture,
    "status": statuss,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

class User {
  User({
    this.id,
    this.mobile,
    this.identifierCode,
    this.createdAt,
    this.updatedAt,
    this.roleId,
  });

  int? id;
  String? mobile;
  String? identifierCode;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? roleId;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    mobile: json["mobile"],
    identifierCode: json["identifier_code"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    roleId: json["role_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "mobile": mobile,
    "identifier_code": identifierCode,
    "created_at": createdAt!.toIso8601String(),
    "updated_at": updatedAt!.toIso8601String(),
    "role_id": roleId,
  };
}
