// To parse this JSON data, do
//
//     final captchaObject = captchaObjectFromJson(jsonString);

import 'dart:convert';

CaptchaObject captchaObjectFromJson(String str) => CaptchaObject.fromJson(json.decode(str));

String captchaObjectToJson(CaptchaObject data) => json.encode(data.toJson());

class CaptchaObject {
  CaptchaObject({
    this.sensitive,
    this.key,
    this.img,
  });

  bool? sensitive;
  String? key;
  String? img;

  factory CaptchaObject.fromJson(Map<String, dynamic> json) => CaptchaObject(
    sensitive: json["sensitive"],
    key: json["key"],
    img: json["img"],
  );

  Map<String, dynamic> toJson() => {
    "sensitive": sensitive,
    "key": key,
    "img": img,
  };
}
