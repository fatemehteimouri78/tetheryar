import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tetheryar/Constants.dart';
import 'package:tetheryar/Provider/UserProvider.dart';
class Information extends StatelessWidget {
  const Information({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(
        builder: (context,value,child) {
          return Column(crossAxisAlignment: CrossAxisAlignment.start,mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            part(title: "نام و نام خانوادگی :",desc: value.user!.authentication!.firstName!+" "+ value.user!.authentication!.lastName!),
            part(title: "کد ملی :",desc: value.user!.authentication!.nationalCode),
            part(title: "شماره همراه ثبت شده :",desc: value.user!.authentication!.phoneNumber),
            part(title: "کد معرف جهت دعوت دوستان :",desc: value.user!.user!.identifierCode),
            part(title: "لینک دعوت از دوستان :",desc: value.user!.authentication!.statuss),
            part(title: "تعداد کاربران دعوت شده توسط من :",desc: value.user!.authentication!.id.toString()),
          ],
        );
      }
    );
  }
  Widget part({String? title,String? desc})=>Padding(
    padding: const EdgeInsets.all(8.0),
    child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: [
      Text(title!,style: TextStyle(fontSize: 18,fontWeight: FontWeight.w600),),
      Text(desc!,style: TextStyle(color: darkColor2,fontSize: 16,fontWeight: FontWeight.w500),)
    ],),
  );
}
