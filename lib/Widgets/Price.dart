import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Price extends StatelessWidget {
  final int? price;
  final Color? myColor;
  final double? mySize;
  const Price({Key? key, this.price,this.myColor, this.mySize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(convertMoney(price!),
            style: TextStyle(color: myColor,fontWeight: FontWeight.w600,fontSize: mySize)),
        Text(" تومان",
            style: TextStyle(color: myColor,fontWeight: FontWeight.w600,fontSize: mySize!+1)),
      ],
    );
  }
  convertMoney(int money) {
    return NumberFormat("#,##0", "en_US").format(money);
  }
}
