import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tetheryar/Screens/Loading.dart';
import 'package:tetheryar/Screens/MainScreen.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';

import '../Constants.dart';
import 'Price.dart';

class CustomCard extends StatelessWidget {
  final String? text;
  final String? textButton;
  final Widget? secondPage;
  final Color? colors;
  final int? price;
  final bool? buy;

  const CustomCard(
      {Key? key,
      this.text,
      this.textButton,
      this.secondPage,
      this.colors,
      this.price,
      this.buy = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: MediaQuery.of(context).size.height / 5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(text!,
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 20)),
                price == 0 ? Loading():
                Price(
                  price: price!,
                  mySize: 35,
                  myColor: buy! ? paleColor : Colors.red,
                ),
                CustomButton(
                  colors: colors,
                  textButton: textButton,
                  border: 5,
                  background: Colors.white,
                  function: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MainScreen(screen: secondPage)));
                    return false;
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
