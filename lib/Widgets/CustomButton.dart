import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tetheryar/Screens/MainScreen.dart';
import 'CustomLoading.dart';

class CustomButton extends StatefulWidget {
  final double? border;
  final bool? maximum;
  final Color? colors;
  final Color?  background;
  final String? textButton;
  final Function? function;

  const CustomButton(
      {Key? key,
      this.colors,
      this.textButton,
      this.border = 5,
      this.maximum = false,
      this.background= Colors.white,
        this.function})
      : super(key: key);

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 60),
      child: GestureDetector(
        child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: widget.colors!),
                color: widget.background, borderRadius: BorderRadius.circular(widget.border!)),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: widget.maximum! ? MainAxisSize.max : MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: isLoading ? LoadingIndicatorWidget(): Text(widget.textButton!,
                        style: TextStyle(
                            color: widget.background == Colors.white ? widget.colors:Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w700)),
                  ),
                ],
              ),
            )),
        onTap: () async {
          setState(() {
            isLoading = true;
          });
          print("aaaa");
          print(isLoading);
         isLoading = await widget.function!()??true;
         setState(() {
         });
          print("bbbb");
          print(isLoading);

        },
      ),
    );
  }
}
