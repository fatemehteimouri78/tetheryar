import 'package:flutter/material.dart';
class MainCard extends StatelessWidget {
  final Widget? screen;
  const MainCard({Key? key, this.screen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 50, 8, 8),
      child: SingleChildScrollView(
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)),
            elevation: 10,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 40,horizontal: 10),
              child: screen,
            )),
      ),
    );
  }
}
