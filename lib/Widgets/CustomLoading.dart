import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

import '../Constants.dart';
class LoadingIndicatorWidget extends StatelessWidget {
  const LoadingIndicatorWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      child: LoadingIndicator(
        indicatorType: Indicator.ballPulse,
        colors: [Colors.blue],
      ),
    );
  }
}
