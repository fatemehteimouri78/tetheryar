import 'package:flutter/material.dart';
class CustomDropDown extends StatefulWidget {
  final List<String>? list;
  final String? text;
  final Function? function;
  const CustomDropDown({Key? key, this.list, this.text, this.function}) : super(key: key);

  @override
  _CustomDropDownState createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  String? value;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(widget.text!,
                style: TextStyle(fontSize: 17
                    ,fontWeight: FontWeight.w600)),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 2),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: Colors.grey.shade300, width: 2)),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                hint: Text("انتخاب کنید"),
                  items: widget.list!.map(buildMenuItem).toList(),
                  value: value,
                  isExpanded: true,
                  iconSize: 36,
                  icon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                  onChanged: (value) {
                  widget.function!(value);
                    setState(() => this.value = value as String?);
                  }),
            ),
          ),
        ],
      ),
    );
  }

  DropdownMenuItem<String> buildMenuItem(String item) =>
      DropdownMenuItem(value: item, child: Text(item));
}
