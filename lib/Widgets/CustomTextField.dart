import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final bool isFromStep1;
  final String? hintText;
  final String? labelText;
  final String? lastText;
  final TextInputType? keyboardType;
  final Color? suffixColor;
  final Function? onChanged;
  final TextEditingController? textEditingController;

  const CustomTextField(
      {Key? key,
      this.hintText = "",
      this.labelText = "",
      this.textEditingController,
      this.onChanged,
      this.lastText = "",
      this.suffixColor = Colors.red,
      this.keyboardType = TextInputType.text,
      this.isFromStep1=false})
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.labelText!,
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600)),
          SizedBox(height: 20),
          TextFormField(
            cursorRadius: Radius.circular(5),
            textDirection: TextDirection.rtl,
            onChanged: (String value) {
              widget.onChanged!(value);
            },
            keyboardType: widget.keyboardType,
            cursorColor: Colors.lightBlueAccent,
            controller: widget.textEditingController,
            style: TextStyle(fontSize: 14),
            decoration: InputDecoration(
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 2),
                borderRadius: BorderRadius.circular(5),
              ),
              suffixIcon: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(widget.lastText!,
                    style: TextStyle(color: widget.suffixColor, fontSize: 17)),
              ),
              hintText: widget.hintText,
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey.shade300, width: 2),
                borderRadius: BorderRadius.circular(5),
              ),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xffc5dffb), width: 3),
                  borderRadius: BorderRadius.circular(7)),
            ),
            validator: (text) {
              if (text == null || text.isEmpty) {
                return "این فیلد نمیتواند خالی باشد!";
              }
              return null;
            },
          ),
        ],
      ),
    );
  }
}
