

import 'package:flutter/material.dart';

class CustomWidget {
  static SnackBar snackBar({
    required String title,
    Color? color,
    TextStyle? style,
  }) {
    return SnackBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      duration: Duration(seconds: 2),
      margin: EdgeInsets.all(16),
      behavior: SnackBarBehavior.floating,
      backgroundColor: color == null ? Colors.red : color,
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 17),
      elevation: 0,
      content: Text(
        title,
          textAlign: TextAlign.center,
        style: style == null ? TextStyle(color: Colors.white) : style,
      ),
    );
  }
}