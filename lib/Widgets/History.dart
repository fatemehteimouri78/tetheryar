// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// class History extends StatefulWidget {
//   final List<String>? columns;
//   const History({Key? key, this.columns}) : super(key: key);
//   @override
//   _HistoryState createState() => _HistoryState();
// }
//
// class _HistoryState extends State<History> {
//   int? sortColumnIndex;
//   bool isAscending = false;
//   List<Log> logs = [];
//
//   @override
//   Widget build(BuildContext context) => ScrollableWidget(child: buildDataTable());
//
//   Widget buildDataTable() {
//     return DataTable(
//       sortAscending: isAscending,
//       sortColumnIndex: sortColumnIndex,
//       columns: getColumns(widget.columns!),
//       rows: getRows(logs),
//     );
//   }
//
//   List<DataColumn> getColumns(List<String> columns) => columns
//       .map((String column) => DataColumn(
//     label: Text(column),
//     onSort: onSort,
//   ))
//       .toList();
//
//   List<DataRow> getRows(List<String> logs) => logs.map((Log log) {
//     final cells = [user.firstName, user.lastName, user.age];
//
//     return DataRow(cells: getCells(cells));
//   }).toList();
//
//   List<DataCell> getCells(List<dynamic> cells) =>
//       cells.map((data) => DataCell(Text('$data'))).toList();
//
//   void onSort(int columnIndex, bool ascending) {
//     if (columnIndex == 0) {
//       logs.sort((user1, user2) =>
//           compareString(ascending, user1.firstName, user2.firstName));
//     } else if (columnIndex == 1) {
//       logs.sort((user1, user2) =>
//           compareString(ascending, user1.lastName, user2.lastName));
//     } else if (columnIndex == 2) {
//       logs.sort((user1, user2) =>
//           compareString(ascending, '${user1.age}', '${user2.age}'));
//     }
//
//     setState(() {
//       this.sortColumnIndex = columnIndex;
//       this.isAscending = ascending;
//     });
//   }
//
//   int compareString(bool ascending, String value1, String value2) =>
//       ascending ? value1.compareTo(value2) : value2.compareTo(value1);
// }
