import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:tetheryar/Screens/Loading.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/apiRequest.dart';

import 'Constants.dart';
import 'Models/CaptchaObj.dart';
import 'Widgets/CustomLoading.dart';

class Captcha extends StatefulWidget {
  final Function? function;
  const Captcha({Key? key, this.function}) : super(key: key);

  @override
  _CaptchaState createState() => _CaptchaState();
}

class _CaptchaState extends State<Captcha> {
  TextEditingController captchaController = TextEditingController();
  bool refresh = false;
  Uint8List? _bytesImage;
  CaptchaObject? captchaObject;
  @override
  void initState() {
    super.initState();
    _getCaptcha();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        captchaObject == null ? Loading(): Padding(
          padding: const EdgeInsets.symmetric(horizontal: 70),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Image.memory(_bytesImage!,fit: BoxFit.cover,),
              refresh ? LoadingIndicatorWidget():IconButton(icon: Icon(Icons.refresh,size: 30,color: darkColor2,),onPressed: () {
                _getCaptcha();
              },)
            ],
          ),
        ),
        CustomTextField(
            keyboardType: TextInputType.number,
            onChanged: (value){
              widget.function!(captchaObject,captchaController.text);
            },
            labelText: "عبارت امنیتی زیر را وارد کنید :",
            hintText: "عبارت امنیتی ",
            textEditingController: captchaController),
      ],
    );
  }

  void _getCaptcha() async{
    setState(() {
      refresh = true;
    });
    await sendRequest(context, RequestTypes.GET, "captcha/api/math", null,
            (Response response) {
          setState(() {
            captchaObject = CaptchaObject.fromJson(response.data);
            _bytesImage = base64.decode(captchaObject!.img!.split(',').last);
            refresh = false;
          });
        });

  }
}
