import 'dart:ui';

const Color darkColor1 =  Color.fromRGBO(62, 98, 216, 1);
const Color darkColor2 =  Color(0xff0c2d8a);
const Color darkColor3 = Color.fromRGBO(27, 39, 79, 1);
const Color paleColor =  Color(0xff50d9af);
const Color fontColor =  Color(0xff353c42);