import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:tetheryar/Provider/PriceProvider.dart';
import 'package:tetheryar/Provider/TetherProvider.dart';
import 'package:tetheryar/Provider/UserProvider.dart';
import 'package:tetheryar/Screens/HomeScreen.dart';
import 'package:tetheryar/Screens/SplashScreen.dart';
import 'Screens/MainScreen.dart';

void main() {
  runApp(TetherYar());
}
class TetherYar extends StatelessWidget {
  const TetherYar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers:[
          ChangeNotifierProvider(create: (_) => UserProvider()),
          ChangeNotifierProvider(create: (_) => PriceProvider()),
          ChangeNotifierProvider(create: (_) => TetherProvider()),
        ],
      child: MaterialApp(
        theme: ThemeData(fontFamily: "IRAN"),
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale('fa', ''),
        ],
        debugShowCheckedModeBanner: false,
        home: SplashScreen(title: '',),
      ),
    );
  }
}
