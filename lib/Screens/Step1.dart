import 'package:digit_to_persian_word/digit_to_persian_word.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:tetheryar/Provider/TetherProvider.dart';
import 'package:tetheryar/Provider/UserProvider.dart';
import 'package:tetheryar/Screens/CreateAccountScreen.dart';
import 'package:tetheryar/Screens/LoginScreen.dart';
import 'package:tetheryar/Screens/Step2_sell.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/Widgets/CustomWidget.dart';
import 'package:tetheryar/Widgets/MainCard.dart';
import 'MainScreen.dart';
import 'Step2_buy.dart';

class StepOne extends StatefulWidget {
  final String? txt;
  final String? title;
  final Color? color;
  final int? price;
  final String? lastText1;
  final String? lastText2;

  const StepOne({
    Key? key,
    this.txt,
    this.title,
    this.price,
    this.color,
    this.lastText1,
    this.lastText2,
  }) : super(key: key);

  @override
  _StepOneState createState() => _StepOneState();
}

class _StepOneState extends State<StepOne> {
  TextEditingController tetherController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  String subtitle = "";
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(builder: (context, value, child) {
      return MainCard(
        screen: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(widget.txt!,
                  style: TextStyle(
                      color: widget.color,
                      fontWeight: FontWeight.w700,
                      fontSize: 25)),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(widget.title!,
                        style: TextStyle(
                            color: Color(0xff353c42),
                            fontWeight: FontWeight.w500,
                            fontSize: 20)),
                    Text(widget.price.toString(),
                        style: TextStyle(
                            color: Color(0xff353c42),
                            fontWeight: FontWeight.w800,
                            fontSize: 20)),
                    Text(" تومان",
                        style: TextStyle(
                            color: Color(0xff353c42),
                            fontWeight: FontWeight.w700,
                            fontSize: 21)),
                  ],
                ),
              ),
              CustomTextField(
                isFromStep1: true,
                labelText: "مقدار تتر مورد نظر را وارد نمایید :",
                hintText: "مقدار تتر",
                textEditingController: tetherController,
                lastText: widget.lastText1!,
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    priceController.value = TextEditingValue(
                        text: (int.parse(value) * widget.price!).toString());
                    subtitle = priceController.text;
                  });
                },
              ),
              SizedBox(height: 20),
              CustomTextField(
                isFromStep1: true,
                labelText: "مبلغ مورد نظر را به تومان وارد کنید :",
                hintText: "مبلغ به تومان",
                textEditingController: priceController,
                lastText: widget.lastText2!,
                suffixColor: widget.color!,
                keyboardType: TextInputType.number,
                onChanged: (String value) {
                  setState(() {
                    tetherController.value = TextEditingValue(
                        text: (int.parse(priceController.text) ~/ widget.price!)
                            .toString());
                    subtitle = value;
                  });
                },
              ),
              Text(
                DigitToWord.toWord(subtitle, StrType.StrWord, isMoney: true),
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.w500),
              ),
              SizedBox(height: 10),
              CustomButton(
                function: () async {
                  if (_formKey.currentState!.validate() && int.parse(tetherController.text) >= 10) {
                    Provider.of<TetherProvider>(context, listen: false).tetherAmount(tetherController.text);
                      if (widget.txt == "خرید تتر") {
                        if (value.isLogin) {
                          if (value.statuss == "approved") {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MainScreen(screen: StepTwoBuy())));
                          } else{
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        MainScreen(screen: CreateAccountScreen(color: widget.color,))));
                          }
                        }else{
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      MainScreen(screen: LoginScreen())));
                        }
                      }else{
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    MainScreen(screen: StepTwoSell(title: widget.txt,))));
                      }
                  }else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      CustomWidget.snackBar(
                        color: Colors.red,
                        title: "حداقل تتر قابل قبول 10 عدد میباشد",
                      ),
                    );
                  }
                  return false;
                },
                background: widget.color,
                maximum: true,
                colors: widget.color!,
                textButton: "تایید",
                border: 3,
              )
            ],
          ),
        ),
      );
    });
  }
}
