import 'package:flutter/material.dart';
import 'package:tetheryar/Widgets/MainCard.dart';

class StepSixSell extends StatelessWidget {
  final String? txt;
  const StepSixSell({Key? key, this.txt}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainCard(
      screen: Text(txt!,style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),
    );
  }
}
