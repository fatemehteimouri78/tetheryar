import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tetheryar/Provider/UserProvider.dart';
import 'package:tetheryar/Screens/CreateAccountScreen.dart';
import 'package:tetheryar/Screens/ProfileScreen.dart';
import 'package:tetheryar/Screens/Step2_buy.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/Widgets/MainCard.dart';
import 'package:tetheryar/apiRequest.dart';

import 'MainScreen.dart';

class OtpCodeScreen extends StatefulWidget {
  final Color? color;
  final bool? isFromAppBar;
  final String? phone;

  const OtpCodeScreen({Key? key, this.color, this.isFromAppBar, this.phone})
      : super(key: key);

  @override
  _OtpCodeScreenState createState() => _OtpCodeScreenState();
}

class _OtpCodeScreenState extends State<OtpCodeScreen> {
  TextEditingController otpCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(
        builder: (context,value,child) {
          return MainCard(
          screen: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CustomButton(
                  border: 8,
                  textButton: "ورود به حساب کاربری",
                  colors: widget.color!),
              CustomTextField(
                onChanged: (value) {},
                hintText: "کد ارسال شده به شماره موبایل ",
                labelText: "کدارسال شده را وارد نمایید",
                textEditingController: otpCodeController,
                keyboardType: TextInputType.number,
              ),
              CustomButton(
                function: () async {
                  await sendRequest(
                      context,
                      RequestTypes.POST,
                      "api/auth/login/verifyOtpCode/" + widget.phone!,
                      {"otpCode": otpCodeController.text},
                      (Response response) async {
                    if (response.statusCode == 422) {
                      ScaffoldMessenger.of(context)
                        ..showSnackBar(
                          SnackBar(
                            content: Text(response.data["message"],
                                textAlign: TextAlign.center),
                            backgroundColor: Theme.of(context).errorColor,
                          ),
                        );
                    } else {
                      SharedPreferences pref = await SharedPreferences.getInstance();
                      pref.setString("token", response.data["token"]);
                      value.getUser(context);
                      value.checkStatus(context);
                      if(widget.isFromAppBar!){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>
                            MainScreen(screen: ProfileScreen())
                        ));
                      }else{
                        if(value.statuss == "approved"){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>
                              MainScreen(screen: StepTwoBuy())
                          ));
                        }else{
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>
                              MainScreen(screen: CreateAccountScreen(color: widget.color,))
                          ));
                        }
                      }
                    }
                  },checkResponse: false);
                  return false;
                },
                colors: widget.color!,
                border: 3,
                textButton: "تایید کد",
                maximum: true,
              )
            ],
          ),
        );
      }
    );
  }
}
