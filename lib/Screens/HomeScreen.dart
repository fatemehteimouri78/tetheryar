import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tetheryar/Constants.dart';
import 'package:tetheryar/Provider/PriceProvider.dart';
import 'package:tetheryar/Widgets/CustomCard.dart';
import 'Step1.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<PriceProvider>(context,listen: false).getPrice(context);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PriceProvider>(
      builder: (context, provider, child) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 50),
          child: ListView(children: [
            CustomCard(
              buy: true,
              text: "قیمت خرید تتر از سایت",
              textButton: "میخواهم تتر بخرم",
              secondPage: StepOne(
                txt: "خرید تتر",
                title: "قیمت خرید تتر ",
                price: provider.buyPrice,
                lastText1: "تتر دریافت میکنید",
                lastText2: "تومان میدهید",
                color: paleColor,
              ),
              colors: paleColor,
              price: provider.buyPrice,
            ),
            CustomCard(
              buy: false,
              text: "قیمت فروش تتر به سایت",
              textButton: "میخواهم تتر بفروشم",
              secondPage: StepOne(
                txt: "فروش تتر",
                title: "قیمت فروش تتر ",
                price: provider.sellPrice,
                lastText1: "تتر میدهید",
                lastText2: "تومان دریافت میکنید",
                color: darkColor2,
              ),
              colors: darkColor2,
              price: provider.sellPrice,

            )
          ]),
        );
      },
    );
  }
}
