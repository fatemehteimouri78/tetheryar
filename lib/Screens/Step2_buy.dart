import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tetheryar/Constants.dart';
import 'package:tetheryar/Models/CaptchaObj.dart';
import 'package:tetheryar/Provider/TetherProvider.dart';
import 'package:tetheryar/Screens/Rules.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/Widgets/CustomWidget.dart';
import 'package:tetheryar/Widgets/MainCard.dart';
import 'package:tetheryar/apiRequest.dart';
import 'package:url_launcher/url_launcher.dart';

import '../CaptchaRequest.dart';

class StepTwoBuy extends StatefulWidget {
  const StepTwoBuy({Key? key}) : super(key: key);

  @override
  _StepTwoBuyState createState() => _StepTwoBuyState();
}

class _StepTwoBuyState extends State<StepTwoBuy> {
  String? _groupValue = "unselected";
  TextEditingController walletController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool checkedValue=false;
  CaptchaObject? captchaObject;
  String? ansr;

  Widget build(BuildContext context) {
    return MainCard(
      screen: Form(
        key: _formKey,
        child: Column(
          children: [
            Text("شبکه تتر مورد نظر را انتخاب کنید"),
            RadioListTile(
              value: "TRC-20",
              groupValue: _groupValue,
              title: Text("TRC-20",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 15),),
              onChanged: (newValue) =>
                  setState(() => _groupValue = newValue as String?),
              activeColor: Colors.red,
              selected: false,
            ),RadioListTile(
              value: "ERC-20",
              groupValue: _groupValue,
              title: Text("ERC-20",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 15)),
              onChanged: (newValue) =>
                  setState(() => _groupValue = newValue as String?),
              activeColor: Colors.red,
              selected: false,
            ),RadioListTile(
              value: "OMNI",
              groupValue: _groupValue,
              title: Text("OMNI",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 15)),
              onChanged: (newValue) =>
                  setState(() => _groupValue = newValue as String?),
              activeColor: Colors.red,
              selected: false,
            ),
            CustomTextField(labelText: "آدرس کیف پول خود را وارد نمایید",hintText: "آدرس",textEditingController: walletController,),
            CheckboxListTile(
              title: RichText(
                text: TextSpan(
                  text: "",
                  style: DefaultTextStyle.of(context).style,
                  children: [
                    TextSpan(text: 'کلیه اطلاعات فوق را تایید کرده و ', style: TextStyle(fontWeight: FontWeight.w600,fontSize: 10)),
                    WidgetSpan(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Rules()));
                          },
                          child: Text(" قوانین و مقررات تتریار ",
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: paleColor,
                              height: 2.5,
                              letterSpacing: 0.7,
                            ),
                          ),
                        )
                    ),
                    TextSpan(text: ' را خوانده و با آن موافق هستم .', style: TextStyle(fontWeight: FontWeight.w600,fontSize: 10)),
                  ],
                ),
              ),
              value: checkedValue,
              onChanged: (newValue) {
                setState(() {
                  checkedValue = newValue!;
                });
              },
              controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
            ),
            Captcha(function: (CaptchaObject captcha,String answer){
              setState(() {
                captchaObject = captcha;
                ansr = answer;
              });
            },),
            CustomButton(textButton: "تایید و اتصال به درگاه",function: (){
              if(_formKey.currentState!.validate()){
                sendRequest(context, RequestTypes.POST, "api/buy/storeBuyRequest",
                    {
                      "tetherAmount": Provider.of<TetherProvider>(context,listen: false).tether,
                      "tether_type":_groupValue,
                      "walletAddress":walletController.text
                    }, (Response response){
                      _launchURLBrowser(response.data);
                    });

              }else if(_groupValue == "unselected"){
                ScaffoldMessenger.of(context).showSnackBar(
                  CustomWidget.snackBar(
                    color: Colors.red,
                    title: "شبکه ی تتر خود را وارد کنید",
                  ),
                );
              }
            },colors: paleColor)
          ],
        ),
      ),
    );
  }
  _launchURLBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
