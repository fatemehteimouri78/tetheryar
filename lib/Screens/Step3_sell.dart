import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tetheryar/Provider/TetherProvider.dart';
import 'package:tetheryar/Screens/Step4_sell.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/CustomDropDown.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/Widgets/CustomWidget.dart';
import 'package:tetheryar/Widgets/MainCard.dart';

import 'MainScreen.dart';

class StepThree extends StatefulWidget {
  final String? phone;
  final String? name;
  final String? email;
  const StepThree(
      {Key? key, this.phone, this.name, this.email})
      : super(key: key);

  @override
  _StepThreeState createState() => _StepThreeState();
}

class _StepThreeState extends State<StepThree> {
  Color color = Color(0xff0c2d8a);
  TextEditingController cardNoController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String? _value;

  @override
  Widget build(BuildContext context) {
    return MainCard(
      screen: Form(
        key: _formKey,
        child: Column(
          children: [
            CustomButton(
                border: 12, textButton: "فروش تتر به سایت", colors: color),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("مقدار تتر قابل فروش شما : "),
                Text(Provider.of<TetherProvider>(context,listen: false).tether + " تتر",
                    style: TextStyle(
                        color: Colors.cyan, fontWeight: FontWeight.w600)),
              ],
            ),
            CustomTextField(
              labelText: "شماره کارت",
              hintText: "شماره کارت",
              textEditingController: cardNoController,
              keyboardType: TextInputType.phone,
              onChanged: (val){},
            ),
            CustomDropDown(
              list: [
                "بانک ملی ایران",
                "بانک سپه",
                "بانک صنعت و معدن",
                "بانک کشاورزی",
                "بانک مسکن",
                "بانک توسعه صادرات ایران",
                "بانک توسعه تعاون",
                "پست بانک ایران",
                "بانک اقتصاد نوین",
                "بانک پارسیان",
                "بانک کارافرین",
                "بانک سامان",
                "بانک سینا",
                "بانک خاورمیانه",
                "بانک شهر",
                "بانک دی",
                "بانک صادرات",
                "بانک ملت",
                "بانک تجارت",
                "بانک رفاه",
                "بانک حکمت ایرانیان",
                "بانک گردشگری",
                "بانک ایران زمین",
                "بانک قوامین",
                "بانک انصار",
                "بانک سرمایه",
                "بانک پاسارگاد",
                "بانک قرض الحسنه مهر ایران",
                "بانک قرض الحسنه رسالت",
              ],
              text: "نام بانک",
              function: (String value) {
                _value = value;
              },
            ),
            CustomButton(
              function: () {
                if(_formKey.currentState!.validate()){
                  if(_value != null){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainScreen(
                              screen: StepFourSell(
                                name: widget.name,
                                phone: widget.phone,
                                bank: _value,
                                cardNo: cardNoController.text,
                                email: widget.email,
                              ),
                            )));
                  }else{
                    ScaffoldMessenger.of(context).showSnackBar(
                      CustomWidget.snackBar(
                        color: Colors.red,
                        title: "بانک مورد نظر را انتخاب کنید",
                      ),
                    );
                  }
                }
                return false;
              },
              maximum: true,
              textButton: "تایید",
              colors: color,
              border: 3,
            )
          ],
        ),
      ),
    );
  }
}
