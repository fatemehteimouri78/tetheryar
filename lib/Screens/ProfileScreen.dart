import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tetheryar/Screens/CreateAccountScreen.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/History.dart';
import 'package:tetheryar/Widgets/Information.dart';
import 'package:tetheryar/Widgets/MainCard.dart';

import 'MainScreen.dart';
import 'Wallet.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Widget body = Information();
  String title = "اطلاعات حساب کاربری";
  int selected = 0;

  List<String> headerTitle = [
    "اطلاعات حساب کاربری",
    "کیف پول من",
    "سوابق خرید تتر در سایت",
    "سوابق فروش تتر به سایت",
    "احراز هویت",
    "کاربران دعوت شده توسط من"
  ];
  List<IconData> headerIcon = [
    Icons.person,
    Icons.account_balance_wallet_rounded,
    Icons.calendar_today_outlined,
    Icons.star,
    Icons.check,
    Icons.people_rounded,
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              itemCount: headerIcon.length,
              itemBuilder: (context, index) => session(
                  icon: headerIcon[index],
                  iconColor: index == selected
                      ? Colors.white
                      : Color.fromRGBO(95, 209, 175, 2),
                  title: headerTitle[index],
                  textColor: index == selected ? Colors.white : Colors.grey,
                  body: bodies(index),
                  cardColor:
                      index == selected ? Color(0xff0c2d8a) : Colors.white,
                  index: index)),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
              child: MainCard(
                  screen: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                    child: Text(
                      title,
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
                    ),
                  ),
                  body,
                ],
              )),
            ),
          )
        ],
      ),
    );
  }

  Widget session(
      {IconData? icon,
      Color? iconColor,
      String? title,
      Color? textColor,
      Widget? body,
      Color? cardColor,
      int? index}) {
    return GestureDetector(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            color: cardColor,
            elevation: 10,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  Icon(icon, color: iconColor, size: 20),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(title!,
                        style: TextStyle(
                            color: textColor, fontWeight: FontWeight.w600)),
                  )
                ],
              ),
            ),
          ),
        ),
        onTap: () {
          setState(() {
            this.selected = index!;
            this.body = body!;
            this.title = title;
          });
        });
  }

  Widget bodies(int index) {
    if (index == 0) {
      return Information();
    } else if (index == 1) {
      return Wallet();
    } else if (index == 2) {
      return Container();
      //   History(title: [
      //   "مقدار درخواستی تتر",
      //   "وجه واریز شده",
      //   "قیمت تتر",
      //   "شبکه تتر",
      //   "آدرس ولت",
      //   "شماره پیگیری",
      //   "TXID",
      //   "وضعیت"
      // ]);
    } else if (index == 3) {
      return Container();
      //   History(title: [
      //   "نام درخواست دهنده",
      //   " شماره شبا",
      //   "بانک",
      //   "قیمت تتر",
      //   "مقدار تتر ارسالی",
      //   "مقدار وجه درخواستی",
      //   "TXID",
      //   "وضعیت تتر ارسالی",
      //   "وضعیت وجه درخواستی",
      //   "شماره پیگیری"
      // ]);
    } else if (index == 4) {
      return CustomButton(
          function: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MainScreen(
                          screen: CreateAccountScreen(color: Color(0xff50d9af)),
                        )));
            return false;
          },
          maximum: true,
          border: 5,
          textButton: "صفحه ی احراز هویت",
          colors: Colors.cyan);
    } else if (index == 5) {
      return Container();
    } else {
      return Container();
    }
  }
}
