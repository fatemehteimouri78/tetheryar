import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';
import 'package:tetheryar/Models/CaptchaObj.dart';
import 'package:tetheryar/Provider/UserProvider.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/CustomDropDown.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/Widgets/CustomWidget.dart';
import 'package:tetheryar/Widgets/MainCard.dart';
import 'package:tetheryar/apiRequest.dart';
import '../CaptchaRequest.dart';
import 'HomeScreen.dart';
import 'MainScreen.dart';

class CreateAccountScreen extends StatefulWidget {
  final Color? color;

  const CreateAccountScreen({Key? key, this.color}) : super(key: key);

  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController familyNameController = TextEditingController();
  TextEditingController codeMelliController = TextEditingController();
  TextEditingController sabetController = TextEditingController();
  TextEditingController codeMoarrefController =
  TextEditingController(text: " ");
  TextEditingController amniatiController = TextEditingController();
  TextEditingController captchaController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  File? image;
  String? value;
  final items = [
    "9 الی 11",
    "11 الی 13",
    "13 الی 15",
    "15 الی 17",
    "17 الی 19",
    "19 الی 21",
    "21 الی 23",
  ];

  CaptchaObject? captchaObject;
  String? ansr;

  Future pickImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;
      final imageTemporary = File(image.path);
      setState(() {
        this.image = imageTemporary;
      });
    } on PlatformException catch (e) {
      print("failed to pick image: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return MainCard(
      screen: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Provider.of<UserProvider>(context).statuss == "not approved"?
            Container(width: MediaQuery
                .of(context)
                .size
                .width, padding: EdgeInsets.all(10),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.red),
              margin: EdgeInsets.all(10),
              child: Text("متاسفانه مدارک شما توسط همکاران ما تایید نشد!",style: TextStyle(fontWeight: FontWeight.w500,fontSize: 17,color: Colors.white),),
            ): Provider.of<UserProvider>(context).statuss == null?
            Container( width: MediaQuery
                .of(context)
                .size
                .width, padding: EdgeInsets.all(10),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.red),
              margin: EdgeInsets.all(10),
              child: Text("مدارک شما توسط همکاران ما در دست بررسی است لطفا تا تایید مدارک خود صبوری نمایید ... با تشکر!",style: TextStyle(fontWeight: FontWeight.w500,fontSize: 17,color: Colors.white),),
            ):Container(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: Text("""
برای خرید لازم است یکبار احراز هویت انجام دهید,
لطفا موارد زیر را با دقت لازم وارد نمایید.
               """),
            ),
            CustomTextField(
              onChanged: (val) {},
              labelText: "نام",
              hintText: "نام مطابق کارت ملی",
              textEditingController: nameController,
            ),
            CustomTextField(
              onChanged: (val) {},
              labelText: "نام خانوادگی",
              hintText: "نام خانوادگی مطابق کارت ملی",
              textEditingController: familyNameController,
            ),
            CustomTextField(
              onChanged: (val) {},
              labelText: "کد ملی",
              hintText: "کد ملی",
              textEditingController: codeMelliController,
              keyboardType: TextInputType.number,
            ),
            CustomTextField(
              onChanged: (val) {},
              labelText: "شماره تماس ثابت",
              hintText: "شماره تماس ثابت",
              textEditingController: sabetController,
              keyboardType: TextInputType.phone,
            ),
            CustomDropDown(
              list: items,
              text: "زمان تماس با تلفن ثابت جهت احراز هویت",
              function: (val) {
                setState(() {
                  value = val;
                });
              },
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
              child: Text("بارگذاری عکس کارت ملی",
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600)),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      child: Container(
                          padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                          child: Text(image != null
                              ? "ویرایش"
                              : "لطفا عکس کارت ملی خود را بارگذاری کنید"),
                          decoration: BoxDecoration(
                              color: Colors.grey[300],
                              border: Border.all(
                                  color: Colors.grey.shade300, width: 2),
                              borderRadius: BorderRadius.circular(5))),
                      onTap: pickImage,
                    ),
                  ),
                  image != null
                      ? Image.file(image!,
                      fit: BoxFit.fill,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width / 3)
                      : Container()
                ],
              ),
            ),
            CustomTextField(
                onChanged: (val) {},
                labelText: "کد معرف (در صورت تمایل)",
                hintText: "در صورت تمایل کد معرف خود را وارد کنید",
                textEditingController: codeMoarrefController),
            Captcha(
              function: (CaptchaObject captcha, String answer) {
                setState(() {
                  captchaObject = captcha;
                  ansr = answer;
                });
              },
            ),
            CustomButton(
                function: () async {
                  if (_formKey.currentState!.validate()) {
                    final mimeTypeData =
                    lookupMimeType(image!.path, headerBytes: [0xff, 0xD8])
                        ?.split('/');
                    await sendRequest(
                        context,
                        RequestTypes.POST,
                        "auth/storeAuth",
                        FormData.fromMap({
                          "first_name": nameController.text,
                          "last_name": familyNameController.text,
                          "nationalCode": codeMelliController.text,
                          "phoneNumber": sabetController.text,
                          "callTime": value,
                          "nationalCardPicture": await MultipartFile.fromFile(
                              image!.path,
                              filename: DateTime
                                  .now()
                                  .millisecondsSinceEpoch
                                  .toString() +
                                  "." +
                                  getFileFormat(image!), contentType: MediaType(
                              mimeTypeData![0], mimeTypeData[1])),
                          "captcha": ansr,
                          "key": captchaObject!.key
                        }), (Response response) {
                      if (response.statusCode == 200) {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    MainScreen(
                                      screen: HomeScreen(),
                                    )));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          CustomWidget.snackBar(
                            color: Colors.red,
                            title: response.data["errors"]
                            ["nationalCardPicture"]
                                .toString() !=
                                "null"
                                ? response.data["errors"]["nationalCardPicture"]
                                .toString()
                                : response.data["errors"]["captcha"]
                                .toString() !=
                                "null"
                                ? response.data["errors"]["captcha"]
                                .toString()
                                : "salam",
                          ),
                        );
                      }
                    }, checkResponse: false);
                  }
                  return false;
                },
                maximum: true,
                border: 3,
                colors: widget.color,
                textButton: "ارسال مدارک")
          ],
        ),
      ),
    );
  }

  String getFileFormat(File file) {
    var name = file.path.split(".");
    print(file.path.split(".")[name.length - 1]);
    return file.path.split(".")[name.length - 1];
  }
}
