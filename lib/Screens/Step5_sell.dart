import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:tetheryar/Constants.dart';
import 'package:tetheryar/Models/WalletObj.dart';
import 'package:tetheryar/Provider/TetherProvider.dart';
import 'package:tetheryar/Screens/Step6_sell.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/Widgets/CustomWidget.dart';
import 'package:tetheryar/apiRequest.dart';

class StepFiveSell extends StatefulWidget {
  final String? phone;
  final List<WalletObject>? wallets;

  const StepFiveSell({Key? key, this.phone, this.wallets}) : super(key: key);

  @override
  _StepFiveSellState createState() => _StepFiveSellState();
}

class _StepFiveSellState extends State<StepFiveSell>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;
  String? _data;
  TextEditingController txIdController = TextEditingController();

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 50, 8, 8),
      child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          elevation: 10,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 10),
            child: Column(
              children: [
                Text(
                  "فروش تتر",
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                      color: Colors.red),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 45,
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(
                      25.0,
                    ),
                  ),
                  child: TabBar(
                    controller: _tabController,
                    indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        25.0,
                      ),
                      color: Colors.indigo,
                    ),
                    labelColor: Colors.white,
                    unselectedLabelColor: Colors.black,
                    tabs: [
                      Tab(
                        text: widget.wallets![0].walletType,
                      ),
                      Tab(
                        text: widget.wallets![1].walletType,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      tabBarPage(
                        wallet: widget.wallets![0],
                      ),
                      tabBarPage(wallet: widget.wallets![1])
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Widget tabBarPage({WalletObject? wallet}) => SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              text: TextSpan(
                text: "",
                style: DefaultTextStyle.of(context).style,
                children: <TextSpan>[
                  TextSpan(
                      text: 'لطفا مقدار ',
                      style: TextStyle(fontWeight: FontWeight.w500)),
                  TextSpan(
                      text: " " +
                          Provider.of<TetherProvider>(context, listen: false)
                              .tether +
                          " تتر ",
                      style: TextStyle(color: paleColor)),
                  TextSpan(
                      text: wallet!.walletType! +
                          " به آدرس ولت زیر واریز نمایید سپس TXID مربوطه را وارد کنید .",
                      style: TextStyle(fontWeight: FontWeight.w500)),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "توجه فرمایید تتر ارسالی پس از کسر کارمزد تعداد " +
                  Provider.of<TetherProvider>(context, listen: false).tether +
                  "تتر باشد .",
              style: TextStyle(color: Colors.red),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "آدرس ولت",
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.grey.shade300,
                  borderRadius: BorderRadius.circular(15)),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(Icons.person),
                  SelectableText(
                    wallet.walletAddress!,
                    style: TextStyle(
                        letterSpacing: 1,
                        fontSize: 13,
                        fontWeight: FontWeight.w600),
                  ),
                  GestureDetector(
                    child: QrImage(
                      data: wallet.walletQrcode!,
                      version: QrVersions.auto,
                      size: 40,
                      gapless: false,
                    ),
                    onTap: (){
                      showDialog(context: context,
                          builder: (BuildContext context) =>
                              AlertDialog(
                                title: Text("اسکن کنید!"),
                                content: Container(
                                  height: MediaQuery.of(context).size.width/2,
                                  width: MediaQuery.of(context).size.width/3,
                                  padding: EdgeInsets.all(5),
                                  child: QrImage(
                                    data: wallet.walletQrcode!,
                                    version: QrVersions.auto,
                                    size: MediaQuery.of(context).size.width/.7,
                                    gapless: false,
                                  ),),
                                actions: [
                                  TextButton(onPressed: () {
                                    Navigator.pop(context);
                                  }, child: Text("بازگشت"))
                                ],
                              ));
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            CustomTextField(
              textEditingController: txIdController,
              hintText: "TXID",
              labelText: "TXID",
              onChanged: (val) {},
            ),
            CustomButton(
              textButton: "تایید",
              colors: darkColor1,
              function: () async {
                if (txIdController.text.isNotEmpty) {
                  await sendRequest(
                      context, RequestTypes.POST, "api/sell/lastStep", {
                    "mobile": widget.phone,
                    "tether_type": wallet.walletType,
                    "txid": txIdController.text
                  }, (Response response) {
                    if (response.statusCode == 200 ||
                        response.statusCode == 201) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => StepSixSell(
                                    txt: response.data["message"],
                                  )));
                    }
                  });
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    CustomWidget.snackBar(
                      color: Colors.red,
                      title: "پر کردن فیلد TXID الزامی است!",
                    ),
                  );
                }
                return false;
              },
              maximum: true,
            )
          ],
        ),
      );
}
