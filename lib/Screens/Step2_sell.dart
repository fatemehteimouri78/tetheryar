import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tetheryar/Constants.dart';
import 'package:tetheryar/Provider/TetherProvider.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/Widgets/MainCard.dart';

import 'MainScreen.dart';
import 'Step3_sell.dart';

class StepTwoSell extends StatelessWidget {
  final String? title;
  const StepTwoSell({Key? key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController phoneNumberController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    final _formKey = GlobalKey<FormState>();
    return MainCard(
      screen: Form(
        key: _formKey,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(title!,
                  style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.w700,
                      fontSize: 25)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("مقدار تتر قابل فروش شما : "),
                Text(" " +Provider.of<TetherProvider>(context,listen: false).tether + " تتر",
                    style: TextStyle(
                        color: Colors.cyan, fontWeight: FontWeight.w600)),
              ],
            ),
            CustomTextField(
              labelText: "نام و نام خانوادگی",
              hintText: "نام و نام خانوادگی مطابق کارت ملی",
              textEditingController: nameController,
              onChanged: (val){},
            ),
            CustomTextField(
              labelText: "شماره همراه",
              hintText: "شماره همراه",
              textEditingController: phoneNumberController,
              keyboardType: TextInputType.phone,
              onChanged: (val){},
            ),
            CustomTextField(
              labelText: "ایمیل",
              hintText: "ایمیل",
              textEditingController: emailController,
              keyboardType: TextInputType.emailAddress,
              onChanged: (val){},
            ),
            CustomButton(
              maximum: true,
              textButton: "تایید",
              colors: paleColor,
              border: 3,
              background: paleColor,
              function: () {
                if (_formKey.currentState!.validate()) {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) =>
                          MainScreen(screen: StepThree(
                            email: emailController.text,
                            name: nameController.text,
                            phone: phoneNumberController.text,),)));
                }
                return false;
              },
            )
          ],
        ),
      ),
    );
  }
}







