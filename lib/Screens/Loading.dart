import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

import '../Constants.dart';

class Loading extends StatelessWidget {
  final bool isFromButton;
  const Loading({Key? key, this.isFromButton = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 40,
        width: 40,
        child: LoadingIndicator(
          indicatorType: Indicator.ballSpinFadeLoader,
          colors: [paleColor],
        ),
      ),
    );
  }
}
