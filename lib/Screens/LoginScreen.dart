import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tetheryar/Models/CaptchaObj.dart';
import 'package:tetheryar/Screens/OtpCodeScreen.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/Widgets/CustomTextField.dart';
import 'package:tetheryar/Widgets/CustomWidget.dart';
import 'package:tetheryar/Widgets/MainCard.dart';
import 'package:tetheryar/apiRequest.dart';
import '../CaptchaRequest.dart';
import 'Loading.dart';
import 'MainScreen.dart';

class LoginScreen extends StatefulWidget {
  final Color? color;
  final bool isFromAppBar;

  const LoginScreen({Key? key, this.color, this.isFromAppBar = false})
      : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController phoneNumberController = TextEditingController();
  CaptchaObject? captchaObject;
  final _formKey = GlobalKey<FormState>();
  String? ansr;
  @override
  Widget build(BuildContext context) {
    return MainCard(
      screen: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CustomButton(
                border: 8,
                textButton: "ورود به حساب کاربری",
                colors: widget.color!),
            CustomTextField(
                keyboardType: TextInputType.phone,
              onChanged: (value){},
                hintText: "شماره موبایل ",
                labelText: "شماره همراه خود را وارد نمایید :",
                textEditingController: phoneNumberController),
            Captcha(function: (CaptchaObject captcha,String answer){
              setState(() {
                captchaObject = captcha;
                ansr = answer;
              });
            },),
            CustomButton(
              function: () async{
                if(_formKey.currentState!.validate()){
                  if(captchaObject != null && ansr != null){
                await sendRequest(context, RequestTypes.POST, "api/auth/login/SendOtpCode",
                    {
                      "mobile":phoneNumberController.text,
                      "captcha": ansr,
                      "key": captchaObject!.key,
                    }, (Response response) {

                  if(response.statusCode == 200){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MainScreen(screen: OtpCodeScreen(
                      phone: phoneNumberController.text,
                      isFromAppBar: widget.isFromAppBar,
                      color: widget.color!,
                    ))));
                  }else{
                    ScaffoldMessenger.of(context).showSnackBar(
                      CustomWidget.snackBar(
                        color: Colors.red,
                        title: "نادرست!",
                      ),
                    );
                  }}
                  ,checkResponse: false);}
              }
                return false;
                },
              colors: widget.color!,
              border: 3,
              textButton: "ارسال کد",
              maximum: true,
            )
          ],
        ),
      ),
    );
  }
}
