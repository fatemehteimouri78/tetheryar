import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tetheryar/Constants.dart';
import 'package:tetheryar/Provider/UserProvider.dart';
import 'package:tetheryar/Screens/About.dart';
import 'package:tetheryar/Screens/ConnectWith.dart';
import 'package:tetheryar/Screens/HomeScreen.dart';
import 'package:tetheryar/Screens/LearnScreen.dart';
import 'package:tetheryar/Screens/LoginScreen.dart';
import 'package:tetheryar/Screens/ProfileScreen.dart';
import 'package:tetheryar/Screens/Rules.dart';
import 'package:tetheryar/Widgets/CustomButton.dart';
import 'package:tetheryar/apiRequest.dart';

import 'SplashScreen.dart';

class MainScreen extends StatefulWidget {
  final Widget? screen;

  const MainScreen({Key? key, this.screen}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool login = false;
  List<String> titles = [
    "صفحه اصلی",
    "ثبت نام/ ورود",
    "آموزش خرید و فروش تتر",
    "قوانین",
    "تماس با ما",
    "درباره ی ما",
  ];
  List<Widget> secondScreens = [
    HomeScreen(),
    LoginScreen(
      color: paleColor,
      isFromAppBar: true,
    ),
    LearnScreen(),
    Rules(),
    ConnectWith(),
    About()
  ];

  @override
  Widget build(BuildContext context) {
    Widget iScreen = widget.screen!;
    Size size = MediaQuery.of(context).size;
    return Consumer<UserProvider>(builder: (context, value, child) {
      return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          key: _scaffoldKey,
          floatingActionButtonLocation:
              FloatingActionButtonLocation.miniStartFloat,
          backgroundColor: darkColor3,
          drawer: Drawer(
            child: Material(
              color: Colors.white,
              child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 40),
                  child: Column(
                    children: [
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: titles.length,
                          itemBuilder: (context, index) {
                            if (value.isLogin == true &&
                                titles[index] == "ثبت نام/ ورود") {
                              return Container();
                            } else {
                              return Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: ListTile(
                                    title: Text(titles[index],
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w700)),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => MainScreen(
                                                    screen:
                                                        secondScreens[index],
                                                  )));
                                    }),
                              );
                            }
                          }),
                      value.isLogin == true
                          ? Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: ListTile(
                                  title: Text("خروج",
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w700)),
                                  onTap: () async {
                                    showModalBottomSheet(
                                        backgroundColor: Colors.transparent,
                                        isScrollControlled: true,
                                        context: context,
                                        builder: (context) => Container(
                                              width: size.width,
                                              height: size.height / 4,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  80),
                                                          topRight:
                                                              Radius.circular(
                                                                  80))),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 15, right: 20),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        IconButton(
                                                            onPressed: () {
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                            icon: Icon(
                                                              Icons.close,
                                                              color: paleColor,
                                                            )),
                                                      ],
                                                    ),
                                                  ),
                                                  Text(
                                                    "آیا میخواهید از حساب کاربری خود خارج شوید؟",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 18,
                                                        color: darkColor3),
                                                  ),
                                                  CustomButton(
                                                    colors: darkColor2,
                                                    maximum: true,
                                                    background: darkColor2,
                                                    textButton: "خروج",
                                                    function: () async {
                                                      SharedPreferences prefs =
                                                          await SharedPreferences
                                                              .getInstance();
                                                      prefs.clear();
                                                      await sendRequest(
                                                          context,
                                                          RequestTypes.DEL,
                                                          "logout",
                                                          null,
                                                          (Response response) {
                                                        if (response
                                                                .statusCode ==
                                                            200) {
                                                          Navigator
                                                              .pushAndRemoveUntil(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              SplashScreen(
                                                                                title: '',
                                                                              )),
                                                                  (route) =>
                                                                      false);
                                                        }
                                                      });
                                                      return false;
                                                    },
                                                  )
                                                ],
                                              ),
                                            ));
                                  }),
                            )
                          : Container()
                    ],
                  )),
            ),
          ),
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            elevation: 0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/tetheryarLogo.png"),
                          fit: BoxFit.fitHeight)),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 2),
                  child: Text("تتر",
                      style: TextStyle(
                          fontSize: 37,
                          color: darkColor3,
                          fontWeight: FontWeight.bold)),
                ),
                Text("یار",
                    style: TextStyle(
                        fontSize: 37,
                        color: paleColor,
                        fontWeight: FontWeight.bold)),
              ],
            ),
            iconTheme: IconThemeData(color: fontColor),
            actions: [
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 2, 2, 0),
                child: Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          _scaffoldKey.currentState!.openDrawer();
                        },
                        icon: Icon(
                          Icons.menu,
                          color: fontColor,
                        )),
                    GestureDetector(
                      child: Container(
                          padding: EdgeInsets.all(8),
                          child: Text(
                              value.isLogin == true
                                  ? value.user!.authentication!.firstName! +
                                      " " +
                                      value.user!.authentication!.lastName!
                                  : " ثبت نام / ورود ",
                              style: TextStyle(
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white,
                                  fontSize: 18)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: darkColor1,
                          )),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MainScreen(
                                    screen: value.isLogin
                                        ? ProfileScreen()
                                        : LoginScreen(
                                            isFromAppBar: true,
                                            color: paleColor))));
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
          body: iScreen,
          floatingActionButton: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white, width: 3)),
              child: FloatingActionButton(
                elevation: 8,
                child: Icon(
                  Icons.headset_mic_rounded,
                  color: Colors.white,
                  size: 26,
                ),
                onPressed: () {},
                backgroundColor: Color(0xff1191d0),
              )),
        ),
      );
    });
  }

  Future<bool> _onWillPop() async {
    Navigator.popUntil(
        context, (Route<dynamic> predicate) => predicate.isFirst);
    return true;
  }
}
