import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../apiRequest.dart';

class PriceProvider extends ChangeNotifier{
  int sellPrice = 0;
  int buyPrice = 0;
  getPrice(BuildContext context) {
    sendRequest(context, RequestTypes.GET, "api/sell/step1", null, (Response response){
        sellPrice = int.parse(response.data["sellPrice"]);
        buyPrice = int.parse(response.data["buyPrice"]);
        notifyListeners();
    });
  }
}