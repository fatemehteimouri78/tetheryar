import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:tetheryar/Models/UserObj.dart';
import 'package:tetheryar/apiRequest.dart';

class UserProvider extends ChangeNotifier {
  UserObject? user;
  String? statuss;
  bool isLogin = false;
  getUser(BuildContext context){
    sendRequest(context, RequestTypes.GET, "api/profile", null, (Response response){
      if(response.statusCode == 200){
        user = UserObject.fromJson(response.data);
        isLogin = true;
        notifyListeners();
      }
    });
  }
  checkStatus(BuildContext context){
    sendRequest(context, RequestTypes.GET, "sell/checkAuth", null, (Response response){
      if(response.statusCode ==200){
        statuss = response.data["authStatus"];
      }
    });
  }
}
